


//Modulos internos
const jwt = require("jsonwebtoken");



//Crear la función del Middleware

function auth (req, res, next) {

	let jwtToken = req.header("Authorization");

	jwtToken = jwtToken.split(" ")[1];

	//Validar si hay un token
	if(!jwtToken) return res.status(400).send("No existe token para validar");

	//Si existe el token
	try{

		const payload = jwt.verify(jwtToken, "clave");
		req.usuario = payload;
		next();

	}  catch (error){

		res.status(400).send("Token no valido, sin autorización");

	}

}

//Exports

module.exports = auth;
